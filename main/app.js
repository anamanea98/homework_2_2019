function addTokens(input, tokens){
    if( typeof input !=='string'){
        throw new Error('Invalid input')
    }
    if(input.length<6){
        throw new Error('Input should have at least 6 characters')
    }
    
    for(i=0;i<tokens.length;i++){
        input=input.replace("...",tokens[i])
    }
    return input
}

const app = {
    addTokens: addTokens
}

module.exports = app;